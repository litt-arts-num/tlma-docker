La Terre et les Lettres au Moyen Age - Point sur le DEV
=======================================================

### Contexte

- Au départ parser un fichier CSV contenant les informations était suffisant pour générer un json pouvant être lu par cytoscape-js (librairie JS pour afficher en graph)
- Puis les données se sont complexifiées au fur et à mesure de l'avancée de l'inventaire de Fleur et de ses comparses
- Il me semble maintenant qu'il faut passer à une gestion des entités via Doctrine et des formulaires
- Le problème est qu'avec cette solution il va falloir entrer les arbres un par un... ça va être fastidieux
- Par contre la mise à jour sera simplifiée

### Données actuelles

- Le CSV à partir du quel le JSON utilisé dans le graph actuel est `application/src/public/nama-data/nama-data-first.csv`
- Le service qui parse le CSV se base sur *ce CSV ne correspond pas au dernières données fournies par Fleur et que l'on a rediscuté!!!*
- Le JSON actuel lu par le graph est `application/src/public/nama-data/nama-data.json`
- Le dernier fichier Excel avec les infos mises à jour est le fichier `SYNTHESE-essai.xlsx`
- Les feuillets 1 et 2 correspondent à ce que m'a fournit Fleur
- Sur le même fichier le feuillet 3 correspond à la nouvelle forme des données que l'on va devoir gérer

### Fonctionnement attendu du Graph

- Au chargement de la page le graphe affiche les données dans leur ensemble
 - Si on vient de la page d'accueil du projet nama, on visualise la liste des arbres recensés. Si on clique sur un item alors on est redirigé sur le graph avec cet item sélectionné
- Au click sur un item du graph on doit aller chercher l'entrée correspondante dans le dictionnaire DFSM (http://www.dfsmed.fr/) et afficher une partie des informations contenues en "local" et récupérées du DFSM. (*POUR L'INSTANT ON AFFICHE UNIQUEMENT LES DONNEES DU DFSM*)
  - Les informations locales à afficher sont : Le(s) type(s) d'arbre associé(s) à l'entrée, le(s) groupe(s), le(s) source(s) (l'abrégé des sources)
  - On affiche ces infos indépendamment des filtres sélectionnés
  - Des définitions du DFSM on ne garde que les domaines botanique et agronomie (propriété mod de l'entrée doit correspondre à "BOT." ou "AGR."
- Ces informations sont affichées dans la colonne de droite.
  - Les définitions provenant du DFSM devraient être dans une "cartouche" toggleable
  - Les infos locales devraient être dans une autre "cartouche" toggleable

#### Filtres pour le graph

- Une "cartouche" filtre toggleable (hidden par défaut) devrait être ajoutée au dessus de la "cartouche" du graph
- Cette cartouche devrait contenir l'ensemble des filtres
- Filtre par langue (choix multiple)
- Filtre par source (choix multiple)
- Filtre par type d'arbre (choix multiple)
- Afficher / cacher les satellites :
  - dénomination de l'arbre en latin
  - dénomination de l'arbre en grec
  - dénomination de l'arbre en "autre langue" (ce sont des langues indéterminées)
  - synonymes
  - autres arbres

#### Champ de recherche

- le champ de recherche doit permettre de chercher des informations en local et dans le DFSM (PAS SUR QUE TOUS SOIT FAIT)
- au click sur un résultat de ce champ de recherche on doit sélectionner l'élément dans le graph et le centrer dans la visualisation (FAIT)
- de ce fait si l'item n'existe pas en local on zappe (car on ne pourra pas le sélectionner)


### Modèle de données

- De manière générale :
  - le modèle de données se base sur les informations présentes dans le fichier excel, feuillet 3 ! Il est commencé... un tout petit peut :p
  - une ligne = une entrée mais pas toujours
    - un arbre peut figurer sur plusieurs lignes avec le même id
    - dans ce cas cela signifie que selon la source dans laquelle l'entrée a été trouvée les informations ne sont pas les mêmes
  - TOUTES les informations complémentaires d'un arbre (latin/grec/autre/autre arbre/synonyme/type d'arbre/groupe) doivent être liées à la source dans laquelle l'information a été trouvée pour pouvoir mettre en place les filtres
  - les ids sont là pour qu'on s'y retrouve dans les données liées
    - la colonne group_ids renvoie vers un ou plusieurs arbres (0-n) répertoriés dans le fichier
    - la colonne source renvoie vers une ou plusieurs sources répertoriées dans le fichier (feuillet 2 du excel)

- Tree c'est ... l'arbre \o/
  - par rapport au fichier de données, si l'entrée french est renseignée alors c'est un arbre en langue Française, ce qui veut dire que les colonnes latin / grec / autre seront des informations satellites
  - si l'entrée n'existe pas en français mais en latin alors c'est un arbre en langue Latine ce qui veut dire que les colonnes grec / autre seront des informations satellites
  - si l'entrée n'existe pas en latin mais en grec... etc.
  - un arbre a donc plusieurs informations liées suivant chaque source :
    - 0-n synonyme(s)
    - 0-n autre arbre
    - 0-n groupe
    - 1-n type d'arbre
    - et suivant les cas des liens :
      - si arbre en français liens possibles vers :
        - un arbre en latin
        - un arbre en grec
        - un arbre en autre
      - si arbre en latin liens possibles vers :
        - un arbre en grec
        - un arbre en autre

- Certaines entités on été créées... Mais pas grand chose... Je laisse le soin à la personne qui me succédera de choisir le nom de ses entités
- Des fixtures sont présentes... il faudra créer les pages de gestion pour ces entrées... ou pas (cf plus bas)
- La tâche la plus ardue restera de sérialiser toutes ces données pour les donner à manger à cytoscape

### Limitation / DFSM

- le dictionnaire DFSM ne dispose *PAS d'API* (http://www.dfsmed.fr/rechercheparentree.php)
- pour l'instant on arrive à récupérer des infos de ce dictionnaire en "hackant" l'url mais ça n'est *pas pérenne!!!*

### Interface d'administration...

- Pour l'instant pas grand chose en place.... J'ai tout juste commencé à faire les entités et les formulaires associés
- Il y a une authentification basique en place, les identifiants sont dans `application/config/packages/security.yml`
- Pour voir le seul formulaire mis en place accéder à la route http://mondomain.fr/admin/create


#### Mise à jour des partenaires / textes pages d'accueil etc.

- de ce côté lé je pense pas qu'il y ait besoin d'un formulaire... Les données vont pas changer tous les jours
- Fleur a expressément signifiée qu'elle n'aurait pas de temps pour mettre à jour les données, donc les interfaces d'administration seront plutôt à l'usage des personnes qui développeront sur ce projet
- donc le / la développeur / développeuse décidera ce dont il/elle a besoin !

### Autres infos en vrac

- Les images dans les bandeaux devraient être au format 2000 * 400 px et devraient être libre de droit etc.
- Le fichier `application/public/nama-data/cy.css` est le fichier css qui style chaque noeud et interraction dans cytoscape.
  - On le charge dans le fichier js `assets/js/nama/graph.js`
