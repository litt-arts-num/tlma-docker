La Terre et les Lettres au Moyen Age
====================================

### Contexte

> TLMA est un projet global qui pour l'instant se limite à NAMA (Nommer les Arbres au Moyen Âge)

### Installation

> nécessite l'installation de docker et docker-compose pour que ça fonctionne (voir la doc https://docs.docker.com/install/)

- cloner le dépôt depuis gitlab et se déplacer dans le répertoire
- `cp .env.dist .env` puis éditer les variables
- `cp application/.env.dist application/.env` puis éditer les variables
- modifier les identifiant / MDP pour le compte admin `config/packages/security.yml`
- `docker-compose up -d`
- lancer l'invite de commande du container apache `docker exec -it tlma-docker_apache_1 /bin/bash`
- depuis cet invite de commande :
  - `composer install`
  - `npm install` && `npm run dev`
- l'application devrait être disponible à l'adresse : `localhost:8083`
- adminer devrait être disponible à l'adresse : `localhost:8086`


### NPM scripts

- `npm run dev` compile les assets
- `npm run watch` compile les assets et les recompile à chaque changement
- `npm run build` compile, minifie et réalise des optimisations pour les assets en production

### Librairies

Utilise plusieurs librairies telles que :

- http://js.cytoscape.org/
- https://github.com/cytoscape/cytoscape.js-panzoom
- https://github.com/running-coder/jquery-typeahead
