/* global require module __dirname */
const Encore = require('@symfony/webpack-encore')
const path = require('path')


Encore
  // the project directory where compiled assets will be stored
  .setOutputPath('public/build/')
  // the public path used by the web server to access the previous directory
  .setPublicPath('/build')
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())

  // define the assets of the project
  .addEntry('js/app', './assets/js/app.js')
  .addEntry('js/graph', './assets/js/nama/graph.js')

  .addStyleEntry('css/app', './assets/css/global.scss')

  .enableSassLoader(function () {}, {
    resolveUrlLoader: false
  })

  // will prefix css properties according to the supported browser set in postcss.config.js
  .enablePostCssLoader()

  .configureBabel(function (babelConfig) {
    babelConfig.presets.push('env')
  })

  .createSharedEntry('vendor', [
    'jquery',
    'bootstrap',
    '@fortawesome/fontawesome',
    '@fortawesome/fontawesome-free-solid',
    '@fortawesome/fontawesome-free-brands',
    '@fortawesome/fontawesome-free-webfonts',
    'jquery-typeahead'
  ])
  .autoProvideVariables({
    'Routing': 'router'
  })


  // for legacy applications that require $/jQuery as a global variable
  .autoProvidejQuery()
  .enableVersioning()


// get "REAL" webpack object
const config = Encore.getWebpackConfig()

config.resolve.alias = {
  'router': path.resolve(__dirname, 'modules/router.js')
}


module.exports = config
