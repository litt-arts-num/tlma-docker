import '@fortawesome/fontawesome-free-solid'
import '@fortawesome/fontawesome'

import cytoscape from 'cytoscape'
import panzoom from 'cytoscape-panzoom'
import options from './modules/options'
import trees from './../../../public/nama-data/nama-data.json'
import sources from './../../../public/nama-data/nama-sources.json'
import viewUtilities from 'cytoscape-view-utilities'

let cy = null
let viewUtils = null
let filters = []
let visible = null
let hidden = null
// register cy add-ons
panzoom(cytoscape, $)
viewUtilities(cytoscape, $)

const urlParams = new URLSearchParams(window.location.search)
const selectedId = urlParams.has('id') ? urlParams.get('id') : null

document.addEventListener('DOMContentLoaded', () => {
  initFiltersEvents()
  // load cy style
  $.ajax({
    url: './../../nama-data/cy.css',
    type: 'GET',
    dataType: 'text'
  }).done((style) => {
    initCy(style, selectedId)
  })
})

const initFiltersEvents = () => {
  // toggle latin nodes filter
  $('#hide-latin').on('change', (e) => {
    if (e.target.checked) {
      filters.push('latin')
    } else {
      filters = filters.filter(filter => filter !== 'latin')
    }
    applyFilters()
  })

  $('#hide-synonyms').on('change', (e) => {
    if (e.target.checked) {
      filters.push('synonym')
    } else {
      filters = filters.filter(filter => filter !== 'synonym')
    }
    applyFilters()
  })
}

const applyFilters = () => {
  hidden = cy.nodes().filter(node => filters.includes(node.data().NodeType))
  visible = cy.nodes().filter(node => !filters.includes(node.data().NodeType))
  viewUtils.hide(hidden)
  viewUtils.show(visible)
  initTypeAhead()
}

const initTypeAhead = () => {
  const treeSearchData = visible.filter(node => node.data().NodeType === 'tree').map(node => {
    const tree = node.data()
    return {
      id: tree.id,
      name: tree.label,
      tree: tree.label
    }
  })

  const synonymsSearchData = visible.filter(node => node.data().NodeType === 'synonym').map(node => {
    const synonym = node.data()
    return {
      id: synonym.id,
      name: synonym.label,
      tree: synonym.tree
    }
  })

  const latinSearchData = visible.filter(node => node.data().NodeType === 'latin').map(node => {
    const latin = node.data()
    return {
      id: latin.id,
      name: latin.label,
      tree: latin.tree
    }
  })

  const searchData = []
  visible.filter(node => node.data().NodeType === 'tree' && node.data().SearchTerms !== '').forEach(node => {
    const data = node.data()

    let SearchTerms = [data.SearchTerms]
    if (data.SearchTerms.includes(',')) {
      SearchTerms = data.SearchTerms.split(',')
    }
    SearchTerms.forEach((term) => {
      searchData.push({
        id: data.id,
        name: term.trim(),
        tree: data.label
      })
    })
  })

  $('.js-typeahead').typeahead({
    //input: '.js-typeahead',
    order: 'desc',
    dynamic: true,
    delay: 500,
    maxItem: 0,
    debug: true,
    source: {
      trees: {
        display: 'name',
        data: treeSearchData
      },
      synonyms: {
        display: 'name',
        data: synonymsSearchData
      },
      latin: {
        display: 'name',
        data: latinSearchData
      },
      dfsm: {
        // search in DFSM dictionary by multiple entries / matched values
        //display: ['auteur.auteur', 'entry.​​lemme.orth', 'syno.ref'],
        display: ['lemme.orth'],
        ajax: (query) => {
          return {
            type: 'GET',
            url: 'http://www.dfsmed.fr/baseXRest.php?run=rechercheparentree.xq',
            data: {
              search: `${query}`
            },
            dataType: 'jsonp',
            callback: {
              done: (data) => {
                return filterDFSMResponse(data)
              },
              fail: (jqXHR) => {
                console.log('Error requesting dfsmed (www.dfsmed.fr)', jqXHR)
                return []
              }
            }
          }
        }
      }
    },
    template: (query, item) => {
      if (item.group === 'trees') {
        return `${item.name}, <small><em>Arbre</em></small>`
      } else if (item.group === 'synonyms') {
        return `${item.name}, <small><em>Synonyme de ${item.tree}</em></small>`
      } else if (item.group === 'latin') {
        return `${item.name}, <small><em>Latin pour ${item.tree}</em></small>`
      } else if (item.group === 'dfsm') {
        return `${item.lemme.orth}, <small><em>DFSM - ${item.med} - ${item.mod}</em></small>`
      } else {
        return `${item.name}, <small><em>Se rapporte à ${item.tree}</em></small>`
      }
    },
    emptyTemplate: 'Aucun résultat pour la requête : {{query}}',
    callback: {
      onClick: (node, a, item) => {
        selectTree(item.id)
      }
    }
  })
}

const selectTree = (id) => {
  const tree = cy.$('#' + id)
  cy.animate({
    zoom: 2,
    center: {
      eles: tree
    }
  }, {
    duration: 500
  })

  cy.nodes().unselect()
  tree.select()
}

/**
 * filterDFSMResponse to match data that is present in our collection
 * It also merge DFSM data and cytoscape data
 * @param  {[object]} raw Response from DFSM website.... not normalized
 * @return {[array]}     an array of object
 */
const filterDFSMResponse = (raw) => {
  if(raw.results && raw.results.parent) {
    if(Array.isArray(raw.results.parent) && raw.results.parent.length > 0) {
      const filtered = raw.results.parent.filter(item =>
        (item.mod === 'AGR.' || item.mod === 'BOT.') && cy.filter((node) =>  node.data().label.toUpperCase() === item.lemme.orth.toUpperCase()).length > 0
      )
      // add id to returned object so that it can select the proper graph node on click
      let mapped = filtered.map(elem => {
        return mergeDfsmData(elem)
      })
      return mapped
    } else {
      const merged = mergeDfsmData(raw.results.parent)
      return [merged]
    }
  } else {
    return []
  }
}

const mergeDfsmData = (elem) => {
  const cyNode = cy.filter((node) =>  node.data().label.toUpperCase() === elem.lemme.orth.toUpperCase())
  // merge DFSM and cytoscape data, among them the id attribute is mandatory
  const merged = cyNode && cyNode.data() ? Object.assign(elem, cyNode.data()) : null
  return merged
}

const initCy = (style, selected) => {

  const cyOptions = Object.assign(options.cytoscape, {
    container: document.getElementById('cy'),
    elements: trees,
    style: style
  })
  cy = cytoscape(cyOptions).ready((cy) => {
    // init add-ons
    cy.target.panzoom(options.panzoom)
    viewUtils = cy.target.viewUtilities(options.viewUtils)

  })
  visible = cy.nodes()
  initTypeAhead()

  const layout = cy.layout(options.layout.cose)

  cy.on('select', 'node', (e) => {
    const node = e.target
    const domNode = $('.details-content')
    domNode.empty()
    // call DFSM
    $.ajax({
      url: 'http://www.dfsmed.fr/baseXRest.php?run=rechercheparentree.xq',
      data: {
        search: node.data().label
      },
      dataType: 'jsonp',
      success: (response) => {
        let nodeDetailTemplate = ''
        const filtered = filterDFSMResponse(response)
        // do we need info from click node (node.data()) ? averything is already visible in the graph...
        if(filtered.length > 0) {
          filtered.forEach(result => {
            nodeDetailTemplate += `
              <div class="card dfsm-entry">
                <div class="card-body">
                  <h5 class="card-title">${result.lemme.orth}</h5>
                  <p class="card-text">
                    <small><em>${result.med} / ${result.mod}</em></small>
                    ${result.gram_pos.gram} ${result.gram_gen.gram} ${result.etymosrc.etymosrc}
                    <hr/>
                    ${result.def.def}
                  </p>
                </div>
              </div>
            `
          })
        } else {
          nodeDetailTemplate = `
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Aucun résultat dans le DFSM</h5>
              </div>
            </div>
          `
        }

        domNode.append(nodeDetailTemplate)

        $('.details-content').show()
        $('.details-content-none').hide()
      },
      error: (msg) => {
        const errorTemplate = `
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Erreur</h5>
              <p class="card-text">
                Une erreur s'est produite en essayant de récupérer des informations du DFSM
              </p>
              <p class="card-text">
                Status : ${msg.status} - Message : ${msg.statusText}
              </p>
              <p class="card-text">
                <a href="www.dfsmed.fr">DFSM, site officiel</a>
              </p>
            </div>
          </div>
        `
        domNode.append(errorTemplate)

        $('.details-content').show()
        $('.details-content-none').hide()
      }
    })
  })

  cy.on('unselect', 'node', () => {
    $('.details-content-none').show()
    $('.details-content').hide()
    // empty search input
    $('.js-typeahead').val('')
  })
  layout.run()
  // if comming from nama home page a tree might have been selected
  if(selected) {
    layout.on('layoutstop', () => {
      selectTree(selected)
    })
  }

}
