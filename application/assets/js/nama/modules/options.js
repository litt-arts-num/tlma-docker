/**
 * Define options for nama js app
 * @type {Object}
 */

const options = {
  cytoscape: {
    motionBlur: true,
    selectionType: 'single',
    boxSelectionEnabled: false,
    autoungrabify: false,
    wheelSensitivity: 0.2
  },
  layout: {
    cose: {
      name: 'cose',
      ready: () => {},
      stop: () => {},
      animate: 'end',
      animationEasing: 'spring(500, 40)',
      animationDuration: 200,
      animateFilter: () => {
        return true
      },
      animationThreshold: 1000,
      refresh: 20,
      fit: true,
      padding: 30,
      boundingBox: undefined,
      nodeDimensionsIncludeLabels: false,
      randomize: false,
      componentSpacing: 100,
      nodeRepulsion: () => {
        return 400000
      },
      nodeOverlap: 10,
      idealEdgeLength: () => {
        return 32
      },
      edgeElasticity: () => {
        return 32
      },
      nestingFactor: 1.2,
      gravity: 1,
      numIter: 1000,
      initialTemp: 1000,
      coolingFactor: 0.99,
      minTemp: 1.0,
      weaver: false
    },
    grid: {
      name: 'grid',
      fit: true,
      padding: 30,
      boundingBox: undefined,
      avoidOverlap: true,
      avoidOverlapPadding: 10,
      nodeDimensionsIncludeLabels: false,
      spacingFactor: undefined,
      condense: false,
      rows: 4,
      cols: undefined,
      position: () => {},
      sort: undefined,
      animate: false,
      animationDuration: 500,
      animationEasing: undefined,
      animateFilter: () => {
        return true
      },
      ready: undefined,
      stop: undefined,
      transform: (node, position) => {
        return position
      }
    },
    breadthfirst: {
      name: 'breadthfirst',
      fit: true,
      directed: false,
      padding: 30,
      circle: false,
      spacingFactor: 1.75,
      boundingBox: undefined,
      avoidOverlap: true,
      nodeDimensionsIncludeLabels: false,
      roots: undefined,
      maximalAdjustments: 0,
      animate: false,
      animationDuration: 500,
      animationEasing: undefined,
      animateFilter: () => {
        return true
      },
      ready: undefined,
      stop: undefined,
      transform: (node, position) => {
        return position
      }
    },
    concentric: {
      name: 'concentric',
      fit: true,
      padding: 30,
      startAngle: 3 / 2 * Math.PI,
      sweep: undefined,
      clockwise: true,
      equidistant: false,
      minNodeSpacing: 10,
      boundingBox: undefined,
      avoidOverlap: true,
      nodeDimensionsIncludeLabels: false,
      height: undefined,
      width: undefined,
      spacingFactor: undefined,
      concentric: (node) => {
        return node.degree()
      },
      levelWidth: (nodes) => {
        return nodes.maxDegree() / 4
      },
      animate: false,
      animationDuration: 500,
      animationEasing: undefined,
      animateFilter: () => {
        return true
      },
      ready: undefined,
      stop: undefined,
      transform: (node, position) => {
        return position
      }
    },
    preset: {
      name: 'preset',
      positions: undefined,
      zoom: undefined,
      pan: undefined,
      fit: true,
      padding: 30,
      animate: false,
      animationDuration: 500,
      animationEasing: undefined,
      animateFilter: () => {
        return true
      },
      ready: undefined,
      stop: undefined,
      transform: (node, position) => {
        return position
      }
    },
    random: {
      name: 'random',
      fit: true,
      padding: 30,
      boundingBox: undefined,
      animate: false,
      animationDuration: 500,
      animationEasing: undefined,
      animateFilter: () => {
        return true
      },
      ready: undefined,
      stop: undefined,
      transform: (node, position) => {
        return position
      }
    }
  },
  panzoom: {
    zoomFactor: 0.05,
    zoomDelay: 45,
    minZoom: 0.1,
    maxZoom: 10,
    fitPadding: 50,
    panSpeed: 10,
    panDistance: 10,
    panDragAreaSize: 75,
    panMinPercentSpeed: 0.25,
    panInactiveArea: 8,
    panIndicatorMinOpacity: 0.5,
    zoomOnly: false,
    fitSelector: undefined,
    animateOnFit: () => {
      return true
    },
    fitAnimationDuration: 1000,

    // icon class names
    sliderHandleIcon: 'fas fa-minus',
    zoomInIcon: 'fas fa-plus',
    zoomOutIcon: 'fas fa-minus',
    resetIcon: 'fas fa-expand'
  },
  viewUtils: {
    node: {
      highlighted: {},
      unhighlighted: {
        'opacity': 0.3
      }
    },
    edge: {
      highlighted: {},
      unhighlighted: {
        'opacity': 0.3
      }
    },
    setVisibilityOnHide: false,
    setDisplayOnHide: true,
    zoomAnimationDuration: 1500,
    neighbor: () => {
      return false
    },
    neighborSelectTime: 500
  }
}

export default options
