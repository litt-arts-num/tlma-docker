<?php

namespace App\Command;

use App\Service\CsvParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Csv2JsonCommand extends Command
{
    private $parser;

    public function __construct(CsvParser $parser)
    {
        $this->paser = $parser;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('nama:csv-2-json')
        ->setDescription('Create JSON Data from CSV for the nama app.')
        ->setHelp('Create JSON Data from CSV.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Parsing CSV',
            '===============',
            '',
        ]);

        $result = $this->parser->parseNamaCsv();

        $output->writeln('done \o/');
    }
}
