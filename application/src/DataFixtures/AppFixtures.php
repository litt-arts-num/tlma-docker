<?php
namespace App\DataFixtures;

use App\Entity\Language;
use App\Entity\Source;
use App\Entity\TreeType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        // create available sources
        $source = new Source();
        $source->setLabel('Simples Médecines 1');
        $source->setDetails('PS. MATTHEUS PLATEARIUS, Le livre des simples médecines : traduction française du "Liber de simplici medicina, dictus Circa instans" de Platearius... , éd. Paul Dorveaux, Paris, Société française d\'histoire de la médecine, 1913.');
        $om->persist($source);

        $source2 = new Source();
        $source2->setLabel('Simples Médecines 2');
        $source2->setDetails('PS. MATTHEUS PLATEARIUS, Livre des simples médecines: Codex Bruxellensis IV 1024, éd. Carmélia Opsomer, 2 vol, Anvers, De Schutter, 1980.');
        $om->persist($source2);

        $source3 = new Source();
        $source3->setLabel('Jardin de santé');
        $source3->setDetails('JOHANNES DE CUBA, Ortus sanitatis, translaté de latin en françois, Paris, pour Antoine Vérard, 1499.');
        $om->persist($source3);

        $source4 = new Source();
        $source4->setLabel('glossaire Lille');
        $source4->setDetails('A. SCHELER, « Glossaire roman-latin du XVè siècle (ms de la Bibliothèque de Lille) », Annales de l\'Académie d\'archéologie de Belgique, 21, 2è série, 1, 1865, pp. 81-133 ');
        $om->persist($source4);

        $source5 = new Source();
        $source5->setLabel('glossaire Douce');
        $source5->setDetails('TONY HUNT, Teaching and Learning Latin in 13th-Century England, Cambridge, D. S. Brewer, 1991, vol. 1, pp. 420-428');
        $om->persist($source5);

        $source6 = new Source();
        $source6->setLabel('glossaire Évreux');
        $source6->setDetails('ALPHONSE-ANTOINE-LOUIS CHASSANT, Petit vocabulaire latin-français du XIIIè siècle, extrait d\'un manuscrit de la bibliothèque d\'Évreux, Paris, A. Aubry, 1857 (nouvelle éd. corrigée 1877), pp. 38-47');
        $om->persist($source6);

        $source7 = new Source();
        $source7->setLabel('glossaire Selinum');
        $source7->setDetails('LÉOPOLD DELISLE, « Note sur un manuscrit de Tours renfermant des gloses françaises du XIIè siècle », Bibliothèque de l\'École des Chartes, 1869, tome 30, pp. 320-333');
        $om->persist($source7);

        $source8 = new Source();
        $source8->setLabel('Hunt');
        $source8->setDetails('TONY HUNT, Plant Names of Medieval England, Cambridge, Brewer, 1989.');
        $om->persist($source8);

        $source9 = new Source();
        $source9->setLabel('Alphita');
        $source9->setDetails('Alphita. Edición crítica y comentario de Alejandro García Gonzáles, Florence, Sismel-Edizioni del Galluzzo, 2007.');
        $om->persist($source9);

        $source10 = new Source();
        $source10->setLabel('MS London, BL, Add. 15236');
        $source10->setDetails('TONY HUNT, « The Botanical Glossaries in MS London BL. Add. 15236 », Pluteus, 4, 1986, pp. 108-135.');
        $om->persist($source10);

        $source11 = new Source();
        $source11->setLabel('MS London, BL, Harley 978');
        $source11->setDetails('WRIGHT, R. P. WÜCKLER, Anglo-Saxon and Old English Vocabularies, London, 1884, vol. 1, pp. 554-559.');
        $om->persist($source11);

        $source12 = new Source();
        $source12->setLabel('missing from fleur 1');
        $source12->setDetails('missing desc from fleur 1');
        $om->persist($source12);

        $source13 = new Source();
        $source13->setLabel('missing from fleur 2');
        $source13->setDetails('missing desc from fleur 2');
        $om->persist($source13);

        $source14 = new Source();
        $source14->setLabel('missing from fleur 3');
        $source14->setDetails('missing desc from fleur 3');
        $om->persist($source14);

        // available tree languages
        $lang1 = new Language();
        $lang1->setLabel('tree_french');
        $om->persist($lang1);

        $lang2 = new Language();
        $lang2->setLabel('tree_latin');
        $om->persist($lang2);

        $lang3 = new Language();
        $lang3->setLabel('tree_greek');
        $om->persist($lang3);

        $lang4 = new Language();
        $lang4->setLabel('tree_other');
        $om->persist($lang4);

        // tree types names
        $treeType1 = new TreeType();
        $treeType1->setLabel('tree_type_with_fruits');
        $om->persist($treeType1);

        $treeType2 = new TreeType();
        $treeType2->setLabel('tree_type');
        $om->persist($treeType2);

        $treeType3 = new TreeType();
        $treeType3->setLabel('tree_type_small_aromatic_with_fruits');
        $om->persist($treeType3);


        // finally flush
        $om->flush();
    }
}
