<?php

namespace App\Form;

use App\Entity\Tree;
use App\Entity\Source;
use App\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

          ->add('label', TextType::class, [
              'label' => 'form_tree_label',
              'translation_domain' => 'forms',
              'required' => true
          ])
          ->add('language', EntityType::class, array(
            'class' => Language::class,
            'label' => 'form_tree_language',
            'translation_domain' => 'forms',
            'choice_label' => 'label',
            'choice_translation_domain' => 'messages',
          ))
          ->add('sources', EntityType::class, array(
            'class' => Source::class,
            'multiple' => true,
            'label' => 'form_tree_sources',
            'translation_domain' => 'forms',
            'choice_label' => 'label',
          ))
          ->add('save', SubmitType::class, array(
              'attr' => array('class' => 'btn btn-primary'),
              'label' => 'form_tree_save',
              'translation_domain' => 'forms',
          ));
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Tree::class,
        ));
    }
}
