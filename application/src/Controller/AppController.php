<?php

namespace App\Controller;

use App\Entity\Tree;
use App\Form\TreeType;
use App\Service\CsvParser;
use App\Service\JsonParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

class AppController extends Controller
{
    private $parser;
    private $translator;
    private $jsonParser;

    public function __construct(CsvParser $parser, JsonParser $jsonParser, TranslatorInterface $translator)
    {
        $this->parser = $parser;
        $this->translator = $translator;
        $this->jsonParser = $jsonParser;
    }

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('app/home.html.twig', []);
    }

    /**
     * @Route("/partners", name="partners")
     */
    public function partners()
    {
        return $this->render('app/partners.html.twig', []);
    }

    /**
     * @Route("/nama", name="nama")
     */
    public function namaHome()
    {
        $trees = $this->jsonParser->getTreeList();

        return $this->render('nama/nama.html.twig', ['trees' => $trees]);
    }

    /**
     * @Route("/nama/graph", name="nama-graph")
     */
    public function namaGraph()
    {
        return $this->render('nama/graph.html.twig', []);
    }

    /**
     * @Route("/nama/admin/create", name="nama-admin-create")
     */
    public function create(Request $request)
    {
        $tree = new Tree();
        $form = $this->createForm(TreeType::class, $tree);

        $form->handleRequest($request);
        return $this->render('nama/create.html.twig', ['form' => $form->createView()]);
    }
}
