<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CsvParser
{
    private $params;
    const FRENCH_TREE = 'french_tree';
    const LATIN_TREE = 'latin_tree';
    const GREEK_TREE = 'greek_tree';
    const OTHER_TREE = 'other_tree';

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * Handle file upload and parse csv data into json file (cytoscape formated)
     * @return boolean
     */
    public function csv2Json(UploadedFile $file)
    {
        $fileName = 'nama-data.csv';
        $basePath = $this->params->get('nama_data_path');
        $file->move($basePath, $fileName);
        // build json based on csv
        $filePath = $basePath.DIRECTORY_SEPARATOR.$fileName;
        $handle = fopen($filePath, 'r');
        $rawCSV = [];
        $keys = [];
        $csvIndex = 0;
        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            if (0 === $csvIndex) {
                $keys = $data;
            } else {
                foreach ($keys as $key => $value) {
                    $elem[$value] = $data[$key];
                }
                $rawCSV[$elem['id']] = $elem;
            }
            $csvIndex++;
        }
        fclose($handle);

        $shaped = $this->shapeRawData($rawCSV);
        $json = json_encode($shaped);
        return file_put_contents($basePath.DIRECTORY_SEPARATOR.'nama-data.json', $json);
    }



    /**
     * Parse csv data into json file (cytoscape formated)
     * The CSV must be in the approptiate folder with the proper name (ie public/nama-data/nama-data.csv)
     * @return boolean
     */
    public function parseNamaCsv()
    {
        $fileName = 'nama-data.csv';
        $basePath = $this->params->get('nama_data_path');

        // build json based on csv
        $filePath = $basePath.DIRECTORY_SEPARATOR.$fileName;
        $handle = fopen($filePath, 'r');
        $rawCSV = [];
        $keys = [];
        $csvIndex = 0;
        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            if (0 === $csvIndex) {
                $keys = $data;
            } else {
                foreach ($keys as $key => $value) {
                    $elem[$value] = $data[$key];
                }
                $rawCSV[$elem['id']] = $elem;
            }
            $csvIndex++;
        }
        fclose($handle);

        $shaped = $this->shapeRawData($rawCSV);
        $json = json_encode($shaped);
        return file_put_contents($basePath.DIRECTORY_SEPARATOR.'nama-data.json', $json);
    }


    private function shapeRawData(array $rawCSV)
    {
        $data = [];
        $ids = 1;
        foreach ($rawCSV as $rawTree) {
            $tree = new \stdClass();
            $tree->data = [
              'id' => 'tree_' . $rawTree['id'],
              'NodeType' => 'tree',
              'label' => $rawTree['lbl_french'],
              'Quality' => 150,
              'SearchTerms' => ''
            ];
            $data[] = $tree;
            // build label latin nodes and edge for the current tree
            if (!empty($rawTree['lbl_latin'])) {
                $labels = explode(',', $rawTree['lbl_latin']);
                foreach ($labels as $label) {
                    $node = new \stdClass();
                    $node->data = [
                      'id' => strval($ids),
                      'NodeType' => 'latin',
                      'label' => trim($label),
                      'tree' => $tree->data['label']
                    ];
                    $data[] = $node;
                    $edge = new \stdClass();
                    $edge->data = [
                      'id' => $tree->data['id'] . '_' . strval($ids),
                      'label' => trim($label) . '_' . $tree->data['label'],
                      'source' => $tree->data['id'],
                      'target' => $node->data['id'],
                      'interaction' => 'lt'
                    ];
                    $data[] = $edge;
                    $ids++;
                }
            }

            // build synonyms nodes and edge for the current tree
            if (!empty($rawTree['synonyms'])) {
                $synonyms = explode(',', $rawTree['synonyms']);
                foreach ($synonyms as $synonym) {
                    $node = new \stdClass();
                    $node->data = [
                      'id' => strval($ids),
                      'NodeType' => 'synonym',
                      'label' => trim($synonym),
                      'tree' => $tree->data['label']
                    ];
                    $data[] = $node;
                    $edge = new \stdClass();
                    $edge->data = [
                      'id' => $tree->data['id'] . '_' . strval($ids),
                      'label' => trim($synonym) . '_' . $tree->data['label'],
                      'source' => $tree->data['id'],
                      'target' => $node->data['id'],
                      'interaction' => 'sy'
                    ];
                    $data[] = $edge;
                    $ids++;
                }
            }

            // build group edge for the current tree
            if (!empty($rawTree['group_id'])) {
                $related = $rawCSV[$rawTree['group_id']];
                $edge = new \stdClass();
                $edge->data = [
                  'id' => $related['id'] . '_' . $tree->data['id'],
                  'label' => $related['lbl_french'] . '_' . $tree->data['label'],
                  'source' => $tree->data['id'],
                  'target' => 'tree_' . strval($related['id']),
                  'interaction' => 'ttt'
                ];

                $data[] = $edge;
            }
        }
        return $data;
    }
}
