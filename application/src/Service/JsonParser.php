<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class JsonParser
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * Read nama json data and return a list of tree
     * @return array
     */
    public function getTreeList()
    {
        $fileName = 'nama-data.json';
        $basePath = $this->params->get('nama_data_path');

        // build json based on csv
        $filePath = $basePath.DIRECTORY_SEPARATOR.'nama-data.json';
        $json = file_get_contents($filePath);
        $data = json_decode($json);

        $trees = array_filter($data, function ($entry) {
            return property_exists($entry->data, 'NodeType') && $entry->data->NodeType === 'tree';
        });

        $grouped = [];
        foreach ($trees as $tree) {
            $key = strtoupper(substr($tree->data->label, 0, 1));
            $grouped[$key][] = ['id' => $tree->data->id, 'label' => $tree->data->label];
        }
        // sort array
        ksort($grouped);
        return $grouped;
    }
}
